const GpioController = require('../../utils/GpioController')

const offLight = async (req, res, next) => {
  try {
    const lightController = new GpioController()
    await lightController.lightOff()
    res.status(200).end()
  } catch (error) {
    console.log('Gpio not detected')
    return next(error)
  }
}

module.exports = offLight
