const GpioController = require('../../utils/GpioController')

const onLight = async (req, res, next) => {
  try {
    const lightController = new GpioController()
    await lightController.lightOn()
    res.status(200).end()
  } catch (error) {
    console.log(error)
    return next(error)
  }
}

module.exports = onLight
