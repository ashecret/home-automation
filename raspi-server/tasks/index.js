const cron = require('node-cron')
const { switchLight } = require('./switch-light')

cron.schedule('30 * * * * *', switchLight)
