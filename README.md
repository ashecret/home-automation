

## Mobile App - Client

To run 
> Change `ip` in `react-app/src/utils/api.js` for ip address of your raspi on local network
```
 cd react-app
 npm install
 npm run start
``` 


## Raspberry Pi - Server 

To run 
```
 cd raspi-server
 npm install
 npm run start
``` 

## UI

![](./app.png)
